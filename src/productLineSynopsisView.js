/**
 * @class {ProductLineSynopsisView}
 */
export default class ProductLineSynopsisView {

    _id:number;

    _name:string;

    _productGroupId:number;

    /**
     * @param {number} id
     * @param {string} name
     * @param {number} [productGroupId]
     */
    constructor(id:number,
                name:string,
                productGroupId:number = null) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        this._productGroupId = productGroupId;

    }

    /**
     * @returns {number}
     */
    get id():number {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get name():string {
        return this._name;
    }

    /**
     *
     * @returns {number}
     */
    get productGroupId():number {
        return this._productGroupId;
    }

}