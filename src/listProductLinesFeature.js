import {inject} from 'aurelia-dependency-injection';
import ProductLineServiceSdkConfig from './productLineServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import ProductLineSynopsisView from './productLineSynopsisView';

@inject(ProductLineServiceSdkConfig, HttpClient)
class ListProductLinesFeature {

    _config:ProductLineServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedProductLines:Array<ProductLineSynopsisView>;

    constructor(config:ProductLineServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Lists all product lines
     * @param {string} accessToken
     * @returns a promise of {ProductLineSynopsisView[]}
     */
    execute(accessToken:string):Promise<Array> {

        if (this._cachedProductLines) {

            return Promise.resolve(this._cachedProductLines);

        }
        else {
            return this._httpClient
                .createRequest('product-lines')
                .asGet()
                .withBaseUrl(this._config.baseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                    // cache
                    this._cachedProductLines =
                        Array.from(
                            response.content,
                            contentItem =>
                                new ProductLineSynopsisView(
                                    contentItem.id,
                                    contentItem.name,
                                    contentItem.productGroupId
                                )
                        );

                    return this._cachedProductLines;

                });
        }
    }
}

export default ListProductLinesFeature;
