import ProductLineServiceSdkConfig from '../../src/productLineServiceSdkConfig';

export default {
    productLineServiceSdkConfig: new ProductLineServiceSdkConfig(
        'https://product-line-service-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ'
}
